				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax()) { 
					// Do nothing	
				}
				// For events
				elseif (is_singular( 'tribe_events' ) ) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Graduate subpage
								wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Events', 'bonestheme' ),
									'menu_class' => 'events-nav',
									'theme_location' => 'events-nav',
									'before' => '',
									'after' => '',
									'depth' => 2,
									'items_wrap' => '<h3>Event Categories</h3> <ul>%3$s</ul>'
								));
							?>
						</nav>
					</div>
				</div>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<? }
				// For posts
				elseif (is_single() || is_category() || is_search() || is_home() ) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'category-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'category-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'grad-nav',
										'theme_location' => 'grad-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(19682) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Department Members subpage
								if (is_tree(1728) || get_field('menu_select') == "department") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'For Department Members', 'bonestheme' ),
									   	'menu_class' => 'department-nav',
									   	'theme_location' => 'department-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>For Department Members</h3> <ul>%3$s</ul>'
									));
								}
								// If an Research subpage
								if (is_tree(1952) || get_field('menu_select') == "research") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Research', 'bonestheme' ),
									   	'menu_class' => 'research-nav',
									   	'theme_location' => 'research-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Research</h3> <ul>%3$s</ul>'
									));
								}
								// If an Applied Linguistics subpage
								if (is_tree(2453) || get_field('menu_select') == "appling") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Applied Linguistics', 'bonestheme' ),
									   	'menu_class' => 'appling-nav',
									   	'theme_location' => 'appling-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Applied Linguistics</h3> <ul>%3$s</ul>'
									));
								}
								// If a Courses subpage
								if (is_tree(1671) || get_field('menu_select') == "courses" 
								// || is_page_template( 'page-undergraduate-courses.php' ) || is_page_template( 'page-graduate-courses.php' )
								) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Courses', 'bonestheme' ),
										'menu_class' => 'courses-nav',
										'theme_location' => 'courses-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Courses</h3> <ul>%3$s</ul>'
									));
								}
								// If a About subpage
								if (is_tree(1704) || get_field('menu_select') == "about" 
								) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>About Us</h3> <ul>%3$s</ul>'
									));
								}
								// If a People subpage
								if (is_tree(1381) || is_singular('people') || is_page_template('page-people-listing.php') || get_field('menu_select') == "people") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'People', 'bonestheme' ),
										'menu_class' => 'people-nav',
										'theme_location' => 'people-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>People</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								if (is_search() || is_404() || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
				<?php } ?>