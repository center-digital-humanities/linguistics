<?php
/*
 Template Name: People Listing
*/
?>
<?php get_header(); ?>
			<div class="content main" id="main-content">
				<div class="col">
					<header>
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>
						<?php // Select what people category to show
						$people_category = get_field('people_category');
						if( $people_category ) {
							$people_cat = $people_category->slug;
						}
						// Set varaibles to decide behavior of page
						if ( get_field('link_to_pages') == 'yes' ) {
							$person_link = 'yes';
						}
						if ( get_field('display_labels') == 'show' ) { 
							$labels = 'yes';
						}
						$people_details = get_field('people_details');
						if( in_array('position', $people_details) ) { 
							$position = 'yes';
						} 
						if( in_array('interest', $people_details) ) {
							$interest = 'yes';
						} 
						if( in_array('email', $people_details) ) {
							$email = 'yes';
						}
						if( in_array('phone', $people_details) ) {
							$phone = 'yes';
						}
						if( in_array('office', $people_details) ) {
							$office = 'yes';
						} 
						if( in_array('pronouns', $people_details) ) {
							$pronouns = 'yes';
						}
						if( in_array('hours', $people_details) ) {
							$hours = 'yes';
						} 
						if( in_array('education', $people_details) ) {
							$education = 'yes';
						} 
						if( in_array('cv', $people_details) ) {
							$cv = 'yes';
						}
						if( in_array('year_span', $people_details) ) {
							$year_span = 'yes';
						} 
						?>
						<?php if ( get_field('display_skip') == 'show' ) { ?>
						<?php 
						$skips = get_field('skip_to_categories');
						if( $skips ): ?>
						<ul class="skip-to">
							<strong>Skip To: </strong>
							<?php foreach( $skips as $term ): ?><li><a href="#<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li><?php endforeach; ?>
						</ul>
						<?php endif; ?>
						<?php } ?>
					</header>
					<div class="people-list">
						<?php if ( $people_cat == "faculty" || $people_cat == "grad" ) {
						// If this is the Faculty page...
						
						$skips = get_field('skip_to_categories');
						if( $skips ): ?>
						<?php foreach( $skips as $term ): 
						$term_slug = $term->slug;
						?>
						<h3 id="<?php echo $term_slug; ?>"><?php echo $term->name; ?></h3>
						<ul class="<?php echo $people_cat ?>">
						<?php $core_loop = new WP_Query( array( 'people_cat' => $term_slug, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
						<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
							<li class="person-item">
								<?php if ( $person_link == 'yes' ) { 
								if( get_field('primary_link') == 'yes') { 
								if(get_field('personal_website')) { ?>
								<a href="<?php the_field('personal_website'); ?>">
								<?php } } else { ?>
								<a href="<?php the_permalink() ?>">
								<?php } } ?>
									<?php // if there is a photo, use it
									if(get_field('photo')) {
										$image = get_field('photo');
										if( !empty($image) ): 
											// vars
											$url = $image['url'];
											$title = $image['title'];
											// thumbnail
											$size = 'people-thumb';
											$thumb = $image['sizes'][ $size ];
											$width = $image['sizes'][ $size . '-width' ];
											$height = $image['sizes'][ $size . '-height' ];
									endif; ?>
									<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php // otherwise use a silhouette
									} else { ?>
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
									<?php } ?>
								<?php if ( $person_link == 'yes' ) { ?>
								</a>
								<?php } ?>
								<dl>
									<dt class="name">
										<h3>
										<?php if ( $person_link == 'yes' ) { 
										if( get_field('primary_link') == 'yes') { 
										if(get_field('personal_website')) { ?>
										<a href="<?php the_field('personal_website'); ?>" class="external">
										<?php } } else { ?>
										<a href="<?php the_permalink() ?>">
										<?php } } ?>
											<?php the_title(); ?>
										<?php if ( $person_link == 'yes' ) { ?>
										</a>
										<?php } ?>
										</h3>
									</dt>
									<?php 
									if ( $position == 'yes' ) {
									if(get_field('position_title')) { ?>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php }
									}
									if ( $year_span == 'yes' ) {
									if(get_field('year_span')) { ?>
									<dd class="year"><?php the_field('year_span'); ?></dd>
									<?php }
									}
									if ( $education == 'yes' ) {
									if(get_field('education')) { ?>
									<dd class="education"><?php the_field('education'); ?></dd>
									<?php }
									}
									if ( $email == 'yes' ) { 
									if(get_field('email_address')) {
										$person_email = get_field('email_address'); ?>
									<dd class="email">
										<?php if ( $labels == 'yes' ) { ?><strong>Email:</strong><?php } ?> <a href="mailto:<?php echo antispambot( $person_email ); ?>"><?php echo antispambot( $person_email ); ?></a>
									</dd>
									<?php }
									}
									if ( $phone == 'yes' ) { 
									if(get_field('phone_number')) { ?>
									<dd class="phone"><?php if ( $labels == 'yes' ) { ?><strong>Phone:</strong><?php } ?> <?php the_field('phone_number'); ?></dd>
									<?php } 
									}
									if ( $office == 'yes' ) { 
									if(get_field('office')) { ?>
									<dd class="office"><?php if ( $labels == 'yes' ) { ?><strong>Office:</strong><?php } ?> <?php the_field('office'); ?></dd>
									<?php } 
									}
									if ( $hours == 'yes' ) { 
									if(get_field('office_hours')) { ?>
									<dd class="hours"><?php if ( $labels == 'yes' ) { ?><strong>Office Hours:</strong><?php } ?> <?php the_field('office_hours'); ?></dd>
									<?php } 
									}
									if ( $pronouns == 'yes' ) { 
									if(get_field('pronouns')) { ?>
									<dd class="pronouns"><?php if ( $labels == 'yes' ) { ?><strong>Pronouns:</strong><?php } ?> <?php the_field('pronouns'); ?></dd>
									<?php } 
									}
									if ( $interest == 'yes' ) {
									if(get_field('interest')) { ?>
									<dd class="interest"><?php if ( $labels == 'yes' ) { ?><strong>Research Interests:</strong><?php } ?> <?php the_field('interest'); ?></dd>
									<?php }
									} ?>
									<?php if ( $person_link == 'yes' ) {
									if( get_field('primary_link') == 'yes') { 
									if(get_field('personal_website')) { ?>
									<a href="<?php the_field('personal_website'); ?>" class="btn external">Visit Website
									<?php } } else { ?>
									<a href="<?php the_permalink() ?>" class="btn">
									Read More
									<?php } 
									}
									if ( $person_link == 'yes' ) { ?>
									</a>
									<?php }
									if ( $cv == 'yes' ) { 
									if(get_field('cv')) { ?>
									<span class="cv download"><a href="<?php the_field('cv'); ?>">Download CV</a></span>
									<?php } 
									} ?>
								</dl>
							</li>
						<?php endwhile; ?>
						</ul>
						<?php endforeach;
						endif;
						} else { 
						// If anything other than the faculty page...
						?>
							<ul class="<?php echo $people_cat ?>">
							<?php $core_loop = new WP_Query( array( 'people_cat' => $people_cat, 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
							<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
								<li class="person-item">
									<?php if ( $person_link == 'yes' ) { 
									if( get_field('primary_link') == 'yes') { 
									if(get_field('personal_website')) { ?>
									<a href="<?php the_field('personal_website'); ?>">
									<?php } } else { ?>
									<a href="<?php the_permalink() ?>">
									<?php } } ?>
										<?php // if there is a photo, use it
										if(get_field('photo')) {
											$image = get_field('photo');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'people-thumb';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
										endif; ?>
										<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php // otherwise use a silhouette
										} else { ?>
										<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php } ?>
									<?php if ( $person_link == 'yes' ) { ?>
									</a>
									<?php } ?>
									<dl>
										<dt class="name">
											<h3>
											<?php if ( $person_link == 'yes' ) { 
											if( get_field('primary_link') == 'yes') { 
											if(get_field('personal_website')) { ?>
											<a href="<?php the_field('personal_website'); ?>" class="external">
											<?php } } else { ?>
											<a href="<?php the_permalink() ?>">
											<?php } } ?>
												<?php the_title(); ?>
											<?php if ( $person_link == 'yes' ) { ?>
											</a>
											<?php } ?>
											</h3>
										</dt>
										<?php 
										if ( $position == 'yes' ) {
										if(get_field('position_title')) { ?>
										<dd class="position"><?php the_field('position_title'); ?></dd>
										<?php }
										}
										if ( $year_span == 'yes' ) {
										if(get_field('year_span')) { ?>
										<dd class="year"><?php the_field('year_span'); ?></dd>
										<?php }
										}
										if ( $education == 'yes' ) {
										if(get_field('education')) { ?>
										<dd class="education"><?php the_field('education'); ?></dd>
										<?php }
										}
										if ( $email == 'yes' ) { 
										if(get_field('email_address')) {
											$person_email = get_field('email_address'); ?>
										<dd class="email">
											<?php if ( $labels == 'yes' ) { ?><strong>Email:</strong><?php } ?> <a href="mailto:<?php echo antispambot( $person_email ); ?>"><?php echo antispambot( $person_email ); ?></a>
										</dd>
										<?php }
										}
										if ( $phone == 'yes' ) { 
										if(get_field('phone_number')) { ?>
										<dd class="phone"><?php if ( $labels == 'yes' ) { ?><strong>Phone:</strong><?php } ?> <?php the_field('phone_number'); ?></dd>
										<?php } 
										}
										if ( $office == 'yes' ) { 
										if(get_field('office')) { ?>
										<dd class="office"><?php if ( $labels == 'yes' ) { ?><strong>Office:</strong><?php } ?> <?php the_field('office'); ?></dd>
										<?php } 
										}
										if ( $hours == 'yes' ) { 
										if(get_field('office_hours')) { ?>
										<dd class="hours"><?php if ( $labels == 'yes' ) { ?><strong>Office Hours:</strong><?php } ?> <?php the_field('office_hours'); ?></dd>
										<?php } 
										}
										if ( $pronouns == 'yes' ) { 
										if(get_field('pronouns')) { ?>
										<dd class="pronouns"><?php if ( $labels == 'yes' ) { ?><strong>Pronouns:</strong><?php } ?> <?php the_field('pronouns'); ?></dd>
										<?php } 
										}
										if ( $interest == 'yes' ) {
										if(get_field('interest')) { ?>
										<dd class="interest"><?php if ( $labels == 'yes' ) { ?><strong>Research Interests:</strong><?php } ?> <?php the_field('interest'); ?></dd>
										<?php }
										} ?>
										<?php if ( $person_link == 'yes' ) {
										if( get_field('primary_link') == 'yes') { 
										if(get_field('personal_website')) { ?>
										<a href="<?php the_field('personal_website'); ?>" class="btn external">Visit Website
										<?php } } else { ?>
										<a href="<?php the_permalink() ?>" class="btn">
										Read More
										<?php } 
										}
										if ( $person_link == 'yes' ) { ?>
										</a>
										<?php }
										if ( $cv == 'yes' ) { 
										if(get_field('cv')) { ?>
										<span class="cv download"><a href="<?php the_field('cv'); ?>">Download CV</a></span>
										<?php } 
										} ?>
									</dl>
								</li>
							<?php endwhile; ?>
							</ul>
						<?php } ?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
<?php get_footer(); ?>