<?php
include('config.php');

/**
 * subject_area_code   array    NULL or empty array includes all subject areas; not case sensitive; example: ('ling')
 * program             string   'U' or 'G', NULL or empty string for both; not case sensitive;
 * quarters            array    NULL or empty array for all; not case sensitive; example: ('18f', '18w')
 * do_not_include      array    classes to remove from results (courseCatalogNumberDisplay) (DO NOT put leading zeros); example: ('165', '165B')
*/

function list_courses_by_subject_area_code(array $subject_area_code = NULL, string $program = NULL, array $quarters = NULL, array $do_not_include = NULL){

    // Create connection
    $db_conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PSWD, DATABASE);

    // Check connection
    if ($db_conn->connect_error) {
        die("Connection failed: " . $db_conn->connect_error);
    } 

    $whereStatement = array();
    //SUBJECT AREA
    if(!empty($subject_area_code) && !is_null($subject_area_code)){
        $whereStatement[] = "sa.code IN ('".implode("', '",$subject_area_code)."')";
    }

    //PROGRAM - default is both
    $program_filter = '';
    if(strcasecmp($program,'U') == 0 || strcasecmp($program,'G') == 0){
        $whereStatement[] = "co.career_level = '".$program."'";
    }

    //QUARTERS
    $quarters_filter = '';
    if(!empty($quarters) && !is_null($quarters)){
        $whereStatement[] = "t.code IN ('".implode("', '",$quarters)."')";
    }

    //DO NOT INCLUDE
    $exclude_class_filter = '';
    if(!empty($do_not_include) && !is_null($do_not_include)){
        $whereStatement[] = "co.catalog_number_display NOT IN ('".implode("', '",$do_not_include)."')";
    }

    $whereFilterString = '';
    if(!empty($whereStatement)){
        $whereFilterString = ' WHERE '.implode(" AND ",$whereStatement);
    }

    $sql = "SELECT
                DISTINCT
                sa.code AS subject_area_code,
                t.id AS term_id,
                t.code AS term_code,
                co.id AS course_id,
                co.career_level AS career_level,
                co.catalog_number,
                co.catalog_number_display AS catalog_number_display,
                co.short_title AS course_short_title,
                co.long_title AS course_long_title,
                co.description AS course_description,
                c.title AS class_title,
                c.description AS class_description,
                i.uclaid,
                i.first_name,
                i.middle_name,
                i.last_name,
                i.full_name
            FROM subject_area AS sa
            LEFT JOIN course AS co
                ON co.subject_area_id = sa.id
            JOIN class AS c   
                ON c.course_id = co.id
            LEFT JOIN term AS t
                ON t.id = c.term_id
           JOIN class_section AS cs
                ON cs.class_id = c.id   
                AND UPPER(cs.enrollment_status) != 'X'
            LEFT JOIN section_instructor AS si
                ON si.class_section_id = cs.id
            LEFT JOIN instructor AS i
                ON i.uclaid = si.uclaid ".
            $whereFilterString;

    $returnedCoures = array();
    $result = $db_conn->query($sql);
    while($row = $result->fetch_assoc()) {
        
        $subjectAreaCode = $row['subject_area_code'];
        $term = $row['term_code'];
        $career_level = $row['career_level'];
        $catalog_number = $row['catalog_number'];
        $catalog_number_display = $row['catalog_number_display'];
        $course_short_title = $row['course_short_title'];
        $course_long_title = $row['course_long_title'];
        $course_description = $row['course_description'];
        //$class_number = $row['class_number'];
        $class_title = $row['class_title'];
        $class_description = $row['class_description'];
        //$section_number = $row['section_number'];
        //$section_id = $row['section_id'];
        $subjectClass = $catalog_number_display.' - '.$class_title;

        $uclaid = $row['uclaid'];
        $first_name = $row['first_name'];
        $middle_name = $row['middle_name'];
        $last_name = $row['last_name'];
        $full_name = $row['full_name'];

        //subjectArea
        if(!isset($returnedCoures[$subjectAreaCode][$subjectClass])){


            $returnedCoures[$subjectAreaCode][$subjectClass] = array(
                'subjectClass' => $subjectClass,
                'careerLevel' => $career_level,
                'subjectAreaCode' => $subjectAreaCode,
                'courseCatalogNumber' => $catalog_number,
                'courseCatalogNumberDisplay' => $catalog_number_display,
                'courseShortTitle' => $course_short_title,
                'courseLongTitle' => $course_long_title,
                'courseDescription' => $course_description,
                //'classNumber' => $class_number,
                'classTitle' => $class_title,
                'classDescription' => $class_description,
                'instructors' => array()
            );
        }

        if(!empty($full_name) && $full_name !== NULL){
            $instructor = array(
                'firstName' => $first_name,
                'middleName' => $middle_name,
                'lastName' => $last_name,
                'fullName' => $full_name
            );

            $returnedCoures[$subjectAreaCode][$subjectClass]['instructors'][] = $instructor;
        }
    }

    $db_conn->close();

    return $returnedCoures;
}