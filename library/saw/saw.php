<?php
ini_set('max_execution_time', 6000);
ini_set('default_socket_timeout', 6000);
ini_set('max_input_time', 6000);


date_default_timezone_set('America/Los_Angeles');
include(__DIR__.'/functions.php');
include(__DIR__.'/config.php');

class SAW {
    
    //API & DB Connections
    protected $esb_token;
    protected $api_endpoint;
    protected $db_conn;
    protected $api_call_counter = 0;

    protected $division_code = 'HU';
    protected $department_code = DEPARTMENT_CODE;

    //Subjct Areas
    protected $subjectAreas = array();
    protected $subject_area_ids = array();

    //Terms
    protected $terms_to_update = array(0,1);
    protected $terms = array();
    protected $term_ids = array();

    //Courses
    protected $courses = array();
    protected $courseSubjCat = array();
    protected $exclude_courses = array('0197', '0198', '0199'); //needs leading zeros, this is the courseCatalogNumber not courseCatalogNumberDisplay
    protected $course_ids = array();

    //Classes
    protected $classes = array();
    protected $class_ids = array();
    protected $display_format = "Official Format";

    //Class Details
    protected $sections = array();

    //Instructors
    protected $instructorUCLAIDs = array();

    //Log info
    protected $data_log_filename;
    protected $api_log_filename;
    protected $api_log_counter = 0;

    //Setup Database
    protected $dbschema_file = __DIR__.'/dbschema.sql';

    public function __construct(){

        $this->setup_log_files();

        $this->set_token();
        $this->set_api_endpoint();

        $this->test_api_connection();
    }

    public function set_department_code($dept_code){
        $this->department_code = $dept_code;
    }

    public function add_academic_year($year){
        if(!in_array($year, $this->terms_to_update)){
            $this->terms_to_update[] = $year;
        }
    }

    public function update_database(){

        $executionStartTime = microtime(true);

        $this->open_db_connection();

        //checks to see if database already set up, if not, it sets it up
        if(!$this->setup_database()){
            $this->close_db_connection();
            exit();
        }

        $this->data_logging('Department: ' . $this->department_code);

        //Terms
        $this->set_terms();
        $this->update_terms();
        $this->set_term_ids();

        //Subject Areas
        $this->set_subject_areas();
        $this->update_subject_areas();
        $this->set_subject_area_ids();

        //Courses
        $this->set_courses();
        $this->update_courses();
        $this->set_course_ids();
        
        //Classes
        $this->set_classes();
        $this->update_classes();
        $this->set_class_ids();

        //Class Sections
        $this->set_sections();
        $this->update_sections();

        //Instructors
        $this->update_instructors();

        $this->close_db_connection();

        $this->data_logging("API CALLS:   ".$this->api_call_counter);

        $executionEndTime = microtime(true);
        $seconds = (float)($executionEndTime - $executionStartTime);
 
        $this->data_logging("This script took $seconds seconds to execute");
    }

    protected function setup_log_files(){
        if(!LOGGING) return;

        $folder = LOG_FOLDER;
        //create folder if it doesnt exist
        if(!is_dir($folder)){
            mkdir($folder);
        }

        $current_time = date('mdY_H_i', time());

        //api logs
        $this->api_log_filename = $folder."/api_log_".$current_time;
        $line = "datetime_start    url \n datetime_complete    total_time(seconds)    request_size(bytes)    size_download(bytes)    primary_ip     local_ip";
        file_put_contents($this->api_log_filename, trim($line).PHP_EOL, FILE_APPEND);

        //data logs
        $this->data_log_filename = $folder."/data_log_".$current_time;
    }

    protected function pre_api_logging($url){
        if(!LOGGING) return;

        $now = new DateTime();
        $datetime = $now->format('m/d/Y h:i:s.u a');

        $line = "- ".++$this->api_log_counter." -\n".$datetime . '    ' . $url;
        file_put_contents($this->api_log_filename, trim($line).PHP_EOL, FILE_APPEND);
    }

    protected function api_logging($ch){
        if(!LOGGING) return;

        $now = new DateTime();
        $datetime = $now->format('m/d/Y h:i:s.u a');

        $info = curl_getinfo($ch);

        $url = $info['url'];
        $request_size = $info['request_size'];
        $total_time = $info['total_time'];
        //$connect_time = $info['connect_time'];
        //$pretransfer_time = $info['pretransfer_time'];

        $size_download = $info['size_download'];
        //$speed_download = $info['speed_download'];
        $primary_ip = $info['primary_ip'];
        //$primary_port = $info['primary_port'];
        $local_ip = $info['local_ip'];
        //$local_port = $info['local_port'];

        $line = '      ' . $datetime . '    ' . $total_time . '    ' . $request_size . '    ' . $size_download . '     ' .$primary_ip. '     '.$local_ip."\n";
        file_put_contents($this->api_log_filename, trim($line).PHP_EOL, FILE_APPEND);
    }

    protected function data_logging($data){

        $now = new DateTime();
        $datetime = $now->format('m/d/Y h:i:s.u a');

        $data = $datetime . ' ' . $data;

        echo $data.newline();

        if(!LOGGING) return;
        //$line = $data;
        file_put_contents($this->data_log_filename, trim($data).PHP_EOL, FILE_APPEND);
    }

    protected function set_token(){

        $esb_ch = curl_init(SAIT_SERVER.':'.SAIT_PORT.'/oauth2/token');
        $options = [
            CURLOPT_PORT => SAIT_PORT,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
            CURLOPT_SSLCERT => CERT_PEM_FILENAME,
            CURLOPT_SSLKEY => CERT_PRIVATE_KEY_FILENAME,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_USERPWD => ESB_API_KEY.':'.ESB_SECRET,
            CURLOPT_HTTPHEADER => ["Cache-control: no-cache", "Content-type: application/x-www-form-urlencoded"],
        ];

        curl_setopt_array($esb_ch, $options);
        $esb_response = curl_exec($esb_ch);

        if (!$esb_response) {
            $error = "Error connecting to ESB: ".curl_error($esb_ch);
            $this->data_logging($error);
            throw new Exception($error);
        }
        $this->data_logging("Connected to ESB");

        $json = json_decode($esb_response);
        curl_close($esb_ch);
        $this->esb_token = $json->access_token;
    }

    protected function set_api_endpoint(){
        $this->api_endpoint = SAIT_SERVER."/sis/api/v1/";
    }

    protected function make_api_call($url_param){

        $url = make_post_friendly($this->api_endpoint.$url_param);
        $this->data_logging($url);
        $sait_ch = curl_init($url); 

        curl_setopt_array($sait_ch, [
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 0,
            CURLOPT_TIMEOUT => 400,
            CURLOPT_HTTPHEADER => ["esmAuthnClientToken: ".$this->esb_token,],
        ]);

        $this->pre_api_logging($url);

        $sait_result = curl_exec($sait_ch);

        $this->api_logging($sait_ch);

        $sait_response = json_decode($sait_result);

        curl_close($sait_ch);

        $this->api_call_counter++;
        return $sait_response;
    }

    public function test_api_connection(){
        $connection = $this->make_api_call('Infrastructure/VerifyConnectivity');

        if($connection == NULL){
            $error = "Unable to connect to SAIT API";
            $this->data_logging($error);
            exit($error);
        }

        $this->data_logging("Connected to SAIT API");
    }

    protected function open_db_connection(){

        // Create connection
        $this->db_conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PSWD);

        // Check connection
        if ($this->db_conn->connect_error) {
            die("Connection failed: " . $this->db_conn->connect_error);
        } 

        //check if database does exist
        $sql = "SELECT COUNT(*) as schema_exists
                    FROM INFORMATION_SCHEMA.SCHEMATA
                    WHERE SCHEMA_NAME = '".DATABASE."'";

        $result = $this->db_conn->query($sql);
        $row = $result->fetch_assoc();
        $schema_exists = $row['schema_exists'];

        if ($schema_exists == 1) {

            $this->close_db_connection();
            $this->db_conn = new mysqli(DB_SERVER, DB_USERNAME, DB_PSWD, DATABASE);

            // Check connection
            if ($this->db_conn->connect_error) {
                die("Connection failed: " . $this->db_conn->connect_error);
            } 
        }

        $this->data_logging("Connected to database");
    }

    protected function close_db_connection(){
        $this->db_conn->close();
    }

    protected function reset_db_connection(){
        $this->close_db_connection();
        $this->open_db_connection();
    }

    protected function setup_database(){

        $success = false;

        //get correct database
        if($this->db_conn->select_db(DATABASE)){  
            $this->data_logging("Database Found: ". DATABASE);

            $success = true;
        }else{
            
            $sql = "CREATE DATABASE IF NOT EXISTS ".DATABASE;

            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Database created successfully");
                $this->reset_db_connection();
                $success = true;
            } else {
                $this->data_logging("Error creating database: " . $this->db_conn->error);
                $success = false;
            }
        }

        if($success){

            //check if database has all the correct tables
            $sql = "SELECT COUNT(*) AS tables_found_count 
                    FROM information_schema.tables 
                    WHERE TABLE_SCHEMA = '".DATABASE."' 
                        AND TABLE_NAME IN ('subject_area', 'term', 'course', 'class', 'class_section', 'instructor', 'section_instructor')";

            $result = $this->db_conn->query($sql);
            $row = $result->fetch_assoc();
            $table_count = $row['tables_found_count'];

            if($table_count == 0){
                //create tables
                $dbschema = file_get_contents($this->dbschema_file);

                $this->data_logging("Setting up database tables...");

                if ($this->db_conn->multi_query($dbschema)) {
                    $this->data_logging("SUCCESS");

                    $this->reset_db_connection();
                    $this->add_academic_year(-1);
                    $success = true;
                }
                else{
                    $this->data_logging("FAIL");
                    $success = false;
                }
            } else if($table_count != 7){
                $this->data_logging("Something went wrong, please manually update/create your tables");
                $success = false;
            }
        }

        if($success){
            if(!$this->get_active_db()){
                $success = false;
            }
        }
        
        return $success;
    }

    protected function get_active_db(){

        $sql='SELECT DATABASE()';

        $result = $this->db_conn->query($sql);

        if($result){
            $row = $result->fetch_row();

            $active_db = $row[0];

            $this->data_logging("Active Database: $active_db");
            $status = true;
        }
        else{
            $this->data_logging("No Active Database");
            $status = false;
        }

        return $status;
    }

    protected function set_terms(){

        $years_to_update = array();

        sort($this->terms_to_update);

        foreach ($this->terms_to_update as $term) {
            $years_to_update[] = get_academic_year_range($term);
        }
        
        $all_terms = array();
        foreach ($years_to_update as $year_range) {

            $jsonTerms = $this->make_api_call('Dictionary/Terms?academicYear='.$year_range.'&PageSize=0');

            if(!is_null($jsonTerms) && !empty($jsonTerms)){

                $terms = $jsonTerms->terms;

                foreach($terms as $term){
                    $all_terms[] = $term->termCode;
                }
            }
        }

        $this->terms = $all_terms;

        $this->data_logging("Term count: ".count($this->terms));
    }

    public function get_terms(){
        return $this->terms;
    }

    protected function set_subject_areas(){

        $division_code = $this->division_code;
        $department_code = $this->department_code;
        $terms = $this->terms;

        $subjectAreaCodes = array();

        $jsonSubjectAreas = $this->make_api_call('Dictionary/SubjectAreas?DivisionCode='.$division_code.'&DepartmentCode='.$department_code.'&PageSize=0');

        if(!is_null($jsonSubjectAreas) && !empty($jsonSubjectAreas)){

            $subjectAreasArray = $jsonSubjectAreas->subjectAreas;

            foreach ($subjectAreasArray as $subjectArea) {

                $subjectAreaLastTermCode = $subjectArea->subjectAreaLastTermCode;
                
                if($subjectAreaLastTermCode != 'None'){
                    $searchEarliestYear = substr($terms[0], 0, 2);
                    $lastTermCodeYear = substr($subjectAreaLastTermCode, 0, 2);
                
                    $searchEarliestYear = (int)DateTime::createFromFormat('y', $searchEarliestYear)->format('Y');
                    $lastTermCodeYear = (int)DateTime::createFromFormat('y', $lastTermCodeYear)->format('Y');
    
                    if($lastTermCodeYear < $searchEarliestYear){
                        continue;
                    }
                }
                
                $subjectAreaCodes[] = $subjectArea->subjectAreaCode;
            }
        }

        $this->subjectAreas = array_unique($subjectAreaCodes);

        $this->data_logging("Subject Areas count: ".count($this->subjectAreas));
    }

    public function get_subject_areas() { 
        return $this->subjectAreas;
    }

    protected function set_courses(){

        $coursesArray = array();

        foreach ($this->subjectAreas as $subjectAreaCode) {
           
            $jsonCourses = $this->make_api_call('Courses?subjectAreaCode='.$subjectAreaCode.'&PageSize=0');

            if(!is_null($jsonCourses) && !empty($jsonCourses)){
                $courses = $jsonCourses->courses;

                foreach ($courses as $course){
                    $catalogNumCollection = $course->courseCatalogNumberCollection;

                    for ($i=0; $i<count($catalogNumCollection); $i++){
                        $currentCatItem = $catalogNumCollection[$i];
                        $currentCatNum = $currentCatItem->courseCatalogNumber;

                        //if its not last one in array
                        if(isset($catalogNumCollection[$i+1])){
                            $nextCatItem = $catalogNumCollection[$i+1];
                            $nextCatNum = $nextCatItem->courseCatalogNumber;
                        
                            //if it has an updated start term code... 
                            //dates in ascending order so if the next catalog number is the same, then an updated version exists
                            if($currentCatNum == $nextCatNum){
                                continue;
                            }
                        }

                        //exclude catalog numbers that begin with identified numbers in $this->exclude_courses
                        foreach ($this->exclude_courses as $exclude_course) {

                            if(stripos($currentCatNum, $exclude_course) === 0){
                                $this->data_logging('SKIPPED '.$currentCatNum);
                                continue 2;
                            }
                        }

                        $catalogNumber = $currentCatNum;
                        $catalogStart = $currentCatItem->courseStartTermCode;

                        //Get Course Details
                        $jsonCoursDetail = $this->make_api_call('Courses/'.$subjectAreaCode.'/'.$catalogNumber.'/'.$catalogStart.'/CourseDetail');
                        
                        if(!is_null($jsonCoursDetail) && !empty($jsonCoursDetail)){
                            $courseDetail = $jsonCoursDetail->courseDetail;

                            $coursesArray[] = $courseDetail;
                            $this->courseSubjCat[] = array('subjectAreaCode' => $subjectAreaCode, 'catalogNumber' => $catalogNumber);
                        }
                    }
                }
            }
        }
        
        $this->courses = $coursesArray;

        $this->data_logging("Courses count: ".count($this->courses));
    }

    protected function set_classes(){

        $classesArray = array();

        foreach ($this->subjectAreas as $subjectAreaCode) {

            foreach($this->terms as $termCode){

                $jsonClasses = $this->make_api_call('Classes/'.$termCode.'?subjectAreaCode='.$subjectAreaCode.'&PageSize=0');

                if(!is_null($jsonClasses) && !empty($jsonClasses)){
                    $classes = $jsonClasses->classes;

                    foreach($classes as $class){
                        
                        //OFFICIAL FORMAT ONLY
                        if($class->classSessionDisplayFormat != $this->display_format){
                            continue;
                        }
                       
                        $courseCatalogNumber = $class->courseCatalogNumber;

                        $current_courseCatalogNumber = array('subjectAreaCode' => $subjectAreaCode, 'catalogNumber'=> $courseCatalogNumber);
                        $courseCatalogNumber_exist = array_keys($this->courseSubjCat, $current_courseCatalogNumber); 

                        // if exists is in array... 
                        if(empty($courseCatalogNumber_exist)){
                            continue;
                        }

                        foreach($class->termSessionGroupCollection as $groupCollection){

                            foreach($groupCollection->classCollection as $collection){
                               
                                $classNum = $collection->classNumber;

                                $jsonClassDetails = $this->make_api_call('Classes/'.$termCode.'/'.$subjectAreaCode.'/'.$courseCatalogNumber.'/'.$classNum.'/classDetail');

                                if(!is_null($jsonClassDetails) && !empty($jsonClassDetails)){

                                    $classDetail = $jsonClassDetails->classDetail;

                                    //OFFICIAL FORMAT ONLY
                                    if($classDetail->classSessionDisplayFormat == $this->display_format){
                                        $classesArray[] = $classDetail;
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
            
        }

        $this->classes = $classesArray;

        $this->data_logging("Classes count: ".count($this->classes));
    }

    protected function set_sections(){

        $classes = $this->classes;
        $allClassSections = array();

        foreach($classes as $class){

            $termCode = $class->offeredTermCode;
            $subjectAreaCode = $class->subjectAreaCode;
            $catalogNum = $class->courseCatalogNumber;
            $classNum = $class->classNumber;

            $jsonClassSections = $this->make_api_call('ClassSections/'.$termCode.'/'.$subjectAreaCode.'/'.$catalogNum.'/'.$classNum);
            
            if(is_null($jsonClassSections) || empty($jsonClassSections)){
                continue;
            }

            //OFFICIAL FORMAT ONLY
            if($jsonClassSections->classSection->classSectionSessionDisplayFormat != $this->display_format){
                continue;
            }

            $classSections =  $jsonClassSections->classSection->classSectionCollection;

            foreach($classSections as $section){

                $sectionNumber = $section->classSectionNumber;
                $jsonSectionDetails = $this->make_api_call('ClassSections/'.$termCode.'/'.$subjectAreaCode.'/'.$catalogNum.'/'.$sectionNumber.'/ClassSectionDetail');
                
                if(!is_null($jsonSectionDetails) && !empty($jsonSectionDetails)){
                    $allClassSectionDetails = $jsonSectionDetails->classSectionDetail;

                    //OFFICIAL FORMAT ONLY
                    if($allClassSectionDetails->classSectionSessionDisplayFormat == $this->display_format){
                        $allClassSectionDetails->classNumber = $classNum;
                        $allClassSections[] = $allClassSectionDetails;
                    }
                }
            }
        }

        $this->sections = $allClassSections;
        $this->data_logging("Sections count: ".count($this->sections));
    }

    public function get_instructor($uclaid){

        $instructorInfo = $this->make_api_call('Instructors/'.$uclaid);
        if(is_null($instructorInfo) || empty($instructorInfo)){
            return null;
        }

        return $instructorInfo->name[0];
    }

    protected function update_terms(){

        //Get all terms
        $termCodes = $this->terms;
        $termsToUpdate = array();

        //get all terms that DONT overlap in DB
        $allTerms = implode("','", $termCodes);
        $sql = "SELECT code FROM term WHERE code IN ('$allTerms')";

        $result = $this->db_conn->query($sql);
       
        if ($result->num_rows > 0) {

            $termsInDB = array();
            while($row = $result->fetch_assoc()) {
                $termsInDB[] = $row['code'];
            }

            $termsToUpdate = array_diff($termCodes, $termsInDB);
        }
        else{
            $termsToUpdate = $termCodes;
        }

        //Insert new terms into DB
        if(empty($termsToUpdate)){
            $this->data_logging("Terms are already up to date");
        }
        else{
            $sql = "INSERT INTO term (code) VALUES('".implode("'),('", $termsToUpdate)."')";

            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Terms have been updated successfully");
            } else {
                $this->data_logging("Error: ".$this->db_conn->error);
            }
        }
    }

    protected function set_term_ids(){
        $termCodes = $this->terms;

        //get all term ids from db
        $allTerms = implode("','", $termCodes);
        $sql = "SELECT id,code FROM term WHERE code IN ('$allTerms')";

        $result = $this->db_conn->query($sql);

        $termIds = array();
        while($row = $result->fetch_assoc()) {
            $id = (string)$row['id'];
            $termIds[$id] = $row['code'];
        }

        $this->term_ids = $termIds;
    }

    protected function update_subject_areas(){

        //Get all subjct areas
        $subjectAreas = $this->subjectAreas;
        $subjectAreasToUpdate = array();

        //get all subjectAreas that do not overlap in DB
        $allSA = implode("','", $subjectAreas);
        $sql = "SELECT code FROM subject_area WHERE code IN ('$allSA')";

        $result = $this->db_conn->query($sql);

        if ($result->num_rows > 0) {

            $SAInDB = array();
            while($row = $result->fetch_assoc()) {
                $SAInDB[] = $row['code'];
            }

            $subjectAreasToUpdate = array_diff($subjectAreas, $SAInDB);
        }
        else{
            $subjectAreasToUpdate = $subjectAreas;
        }

        //Insert new terms into DB
        if(empty($subjectAreasToUpdate)){
            $this->data_logging("Subject areas are already up to date");
        }
        else{
            $sql = "INSERT INTO subject_area (code) VALUES('".implode("'),('", $subjectAreasToUpdate)."')";

            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Subject areas have been updated successfully");
            } else {
                $this->data_logging("Error: " . $this->db_conn->error);
                $this->data_logging($sql);
            }
        }
    }

    protected function set_subject_area_ids(){

        $subjectAreas = $this->subjectAreas;

        //get all term ids from db
        $allSubjctAreas = implode("','", $subjectAreas);
        $sql = "SELECT id,code FROM subject_area WHERE code IN ('$allSubjctAreas')";

        $result = $this->db_conn->query($sql);

        $subjectAreaIds = array();
        while($row = $result->fetch_assoc()) {
            $id = (string)$row['id'];
            $subjectAreaIds[$id] = $row['code'];
        }

        $this->subject_area_ids = $subjectAreaIds;
    }

    protected function update_courses(){
        
        $courses = $this->courses;
        $terms = $this->term_ids;
        $subjectAreaIds = $this->subject_area_ids;

        //get all classes from database to array
        $sql = "SELECT 
                    id, 
                    subject_area_id, 
                    career_level,
                    catalog_number,
                    catalog_number_display,
                    short_title,
                    long_title, 
                    description 
                FROM course";

        $result = $this->db_conn->query($sql);

        $courses_in_db = array();
        while($row = $result->fetch_assoc()) {

            $id = (string)$row['id'];
            $subject_area_id = $row['subject_area_id'];
            $career_level = $row['career_level'];
            $catalog_number = $row['catalog_number'];
            $catalog_number_display = $row['catalog_number_display'];
            $short_title = $row['short_title'];
            $long_title = $row['long_title'];
            $description = $row['description'];


            $courses_in_db[$id] = array(
                'subject_area_id' => $subject_area_id, 
                'career_level' => $career_level,
                'catalog_number'=> $catalog_number, 
                'catalog_number_display' => $catalog_number_display,
                'short_title' => $short_title,
                'long_title' => $long_title,
                'description' => $description
            );
        }

        $courses_to_delete = $courses_in_db;

        $courses_to_add_str = '';
        
        foreach ($courses as $course) {
            $courseSubjAreaCode = $course->subjectAreaCode;

            $courseSubjectAreaId = array_search($courseSubjAreaCode,$subjectAreaIds);
            $courseCareerLevel = $course->courseCareerLevelCode;
            $courseCatalogNumber = $course->courseCatalogNumber;
            $courseCatalogNumberDisplay = $course->courseCatalogNumberDisplay;
            $courseShortTitle = $course->courseShortTitle;
            $courseLongTitle = $course->courseLongTitle;

            $courseDescription = $course->courseDescription;
            $courseDescription = strlen($courseDescription) > 1000 ? substr($courseDescription,0,1000) : $courseDescription;

            //check if this item is in array
            $current_item = array(
                'subject_area_id' => $courseSubjectAreaId, 
                'career_level' => $courseCareerLevel,
                'catalog_number'=> $courseCatalogNumber, 
                'catalog_number_display' => $courseCatalogNumberDisplay,
                'short_title' => $courseShortTitle,
                'long_title' => $courseLongTitle,
                'description' => $courseDescription
            );

            $key = array_keys($courses_to_delete, $current_item); 

            // if exists is in array... 
            if(!empty($key)){
                //remove from array
                unset($courses_to_delete[$key[0]]);
            }
            else{ //if it doesnt exist in array
                //add to database
                $courseShortTitle = addslashes($courseShortTitle);
                $courseLongTitle = addslashes($courseLongTitle);
                $courseDescription = addslashes($courseDescription);

                $courses_to_add_str .= "($courseSubjectAreaId, '$courseCareerLevel', '$courseCatalogNumber', '$courseCatalogNumberDisplay', '$courseShortTitle', '$courseLongTitle', '$courseDescription'),";
            }
        }

        //Delete what is left in array from database course table and propegating tables
        if(!empty($courses_to_delete)) {

            $course_id_string =  implode(", ",array_keys($courses_to_delete));
            $sql = "DELETE FROM course WHERE id in (".$course_id_string.")";
            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Courses deleted successfully");
                //class and class section dependancies are deleted automatically with cascade
            } else {
                $this->data_logging("Error deleting courses: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No courses to delete");
        }

        //Add new items to database!
        if(!empty($courses_to_add_str)){

            $sql = "INSERT INTO course(subject_area_id, career_level, catalog_number, catalog_number_display, short_title, long_title, description) VALUES ".rtrim($courses_to_add_str,",");
            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Courses inserted successfully");
            } else {
                $this->data_logging("Error inserting courses: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No new courses to insert");
        }
    }

    protected function set_course_ids(){

        $subjectAreaIds = $this->subject_area_ids;

        // load all courses from db with all subject areas to array
        //$subjArea_id_string =  implode(", ",array_keys($subjectAreaIds));

        $sql = "SELECT 
                    id, 
                    subject_area_id, 
                    catalog_number
                FROM course";

        $result = $this->db_conn->query($sql);

        $courses_in_db = array();
        while($row = $result->fetch_assoc()) {

            $id = (string)$row['id'];
            $subject_area_id = $row['subject_area_id'];
            $catalog_number = $row['catalog_number'];

            $courses_in_db[$id] = array(
                'subject_area_id' => $subject_area_id, 
                'catalog_number'=> $catalog_number
            );
        }

        $this->course_ids = $courses_in_db;
    }

    protected function update_classes(){
        $classes = $this->classes;
        $subjectAreaIds = $this->subject_area_ids;
        $termIds = $this->term_ids;
        $courseIds = $this->course_ids;

        $term_id_string = implode(", ",array_keys($termIds));

        //get all classes from database to array
        $sql = "SELECT 
                    id, 
                    course_id, 
                    term_id,
                    class_number,
                    title, 
                    description 
                FROM class
                WHERE term_id in (".$term_id_string.")";

        $result = $this->db_conn->query($sql);

        $classes_in_db = array();
        while($row = $result->fetch_assoc()) {
            $id = (string)$row['id'];
            $course_id = $row['course_id'];
            $term_id = $row['term_id'];
            $class_number = $row['class_number'];
            $title = $row['title'];
            $description = $row['description'];

            $classes_in_db[$id] = array('course_id' => $course_id, 'term_id'=>$term_id, 'class_number' => $class_number, 'title'=> $title, 'description' => $description);
        }

        $classes_to_delete = $classes_in_db;
        $classes_to_add_str = '';

        foreach($classes as $class){

            $classNumber = $class->classNumber;
            $classTitle = $class->classTitle;
            $classDescription = $class->classDescription;
            //max 1000 char
            $classDescription = strlen($classDescription) > 1000 ? substr($classDescription,0,1000) : $classDescription;
            
            $termCode = $class->offeredTermCode;
            $subjectAreaCode = $class->subjectAreaCode;
            $catalogNum = $class->courseCatalogNumber;

            //get subject area and term ID
            $subjectAreaId = array_search($subjectAreaCode,$subjectAreaIds);
            $termId = array_search($termCode,$termIds);
            

            //get course id
            $courseId = NULL;
            $class_key = array();
            $current_course_item = array('subject_area_id' => $subjectAreaId, 'catalog_number' => $catalogNum);
            $courseIdArray = array_keys($courseIds, $current_course_item); 

            //get class key
            if(!empty($courseIdArray)){
                $courseId = $courseIdArray[0];

                //check if this item is in array
                $current_class_item = array(
                    'course_id' => $courseId, 
                    'term_id' => $termId, 
                    'class_number' => $classNumber, 
                    'title' => $classTitle, 
                    'description' => $classDescription);

                $class_key = array_keys($classes_to_delete, $current_class_item); 
            }

            // if exists is in array... 
            if(!empty($class_key)){
                //remove from array
                unset($classes_to_delete[$class_key[0]]);
                //--to keep code simpler, if title or description change, we just delete and reimput rather than updating
            }
            else{ //if it doesnt exist in array
                //add to database
                //$courses_to_add[] = $current_item;
                $classTitle = addslashes($classTitle);
                $classDescription = addslashes($classDescription);
                $classes_to_add_str .= "($courseId, $termId, '$classNumber', '$classTitle', '$classDescription'),";
            }
        }

        //Delete what is left in array from database course table and propegating tables
        if(!empty($classes_to_delete)) {

            $classes_id_string =  implode(", ",array_keys($classes_to_delete));
            $sql = "DELETE FROM class WHERE id in (".$classes_id_string.")";
            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Classes deleted successfully");
                //class and class section dependancies are deleted automatically with cascade
            } else {
                $this->data_logging("Error deleting classes: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No classes to delete");
        }

        //Add new items to database!
        if(!empty($classes_to_add_str)){

            $sql = "INSERT INTO class(course_id, term_id, class_number, title, description) VALUES ".rtrim($classes_to_add_str,",");

            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Classes inserted successfully");
            } else {
                $this->data_logging("Error inserting classes: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No new classes to insert");
        }
    }

    protected function set_class_ids(){

        $terms = $this->term_ids;

        // load all courses from db (with all current terms) to array
        $term_id_string =  implode(", ",array_keys($terms));

        //get all classes from database to array
        $sql = "SELECT 
                    id, 
                    course_id, 
                    term_id,
                    class_number
                FROM class
                WHERE term_id in (".$term_id_string.")";

        $result = $this->db_conn->query($sql);

        $classes_in_db = array();
        while($row = $result->fetch_assoc()) {
            $id = (string)$row['id'];
            $course_id = $row['course_id'];
            $term_id = $row['term_id'];
            $class_number = $row['class_number'];

            $classes_in_db[$id] = array('course_id' => $course_id, 'term_id' => $term_id,'class_number'=> $class_number);
        }

        $this->class_ids = $classes_in_db;
    }

    protected function update_sections(){

        $sections = $this->sections;
        $subjectAreaIds = $this->subject_area_ids;
        $termIds = $this->term_ids;
        $courseIds = $this->course_ids;
        $classIds = $this->class_ids;

        $sections_in_db = array();

        $term_id_string = implode(", ",array_keys($termIds));

        //Get currently existing sections
        $sql = "SELECT
                    cs.id,
                    cs.class_id,
                    cs.section_number,
                    cs.section_id,
                    cs.enrollment_status
                FROM class_section AS cs
                JOIN class AS c
                    ON c.id = cs.class_id
                WHERE c.term_id in (".$term_id_string.")";

        $result = $this->db_conn->query($sql);
        while($row = $result->fetch_assoc()) {
            $id = $row['id'];
            $class_id = $row['class_id'];
            $section_number = $row['section_number'];
            $section_id = $row['section_id'];
            $enrollment_status = $row['enrollment_status'];
            
            $sections_in_db[$id] = array(
                'class_id' => $class_id, 
                'section_number' => $section_number, 
                'section_id' => $section_id, 
                'enrollment_status' => $enrollment_status
            );
        }
        //all sections are to be deleted unless they match new api data
        $sections_to_delete = $sections_in_db;


        $section_instr_str = '';
        $section_instr_db = array();
        //get currently existing instructors associated with sections
        if(!empty($sections_in_db)){
            $sql = 'SELECT class_section_id, uclaid FROM section_instructor WHERE class_section_id IN ('.implode(", ",array_keys($sections_in_db)).')';

            $result = $this->db_conn->query($sql);
            while($row = $result->fetch_assoc()) {
                $class_section_id = $row['class_section_id'];
                $uclaid = $row['uclaid'];

                $section_instr_db[$class_section_id][] = $uclaid;
            }
        }
        //all instructors are to be deletes unless they match new api data
        $section_instr_to_delete = $section_instr_db;

        //get new api data
        foreach($sections as $section){
 
            $class_number = $section->classNumber;
            $termCode = $section->offeredTermCode;
            $subjectAreaCode = $section->subjectAreaCode;
            $catalogNum = $section->courseCatalogNumber;
            $sectionNum = $section->classSectionNumber;
            $sectionId = $section->classSectionID;
            $enrollmentStatus = $section->classSectionEnrollmentStatusCode;

            //get subject area and term ID
            $subjectAreaId = array_search($subjectAreaCode,$subjectAreaIds);
            $termId = array_search($termCode,$termIds);

            //get course id
            $courseId = NULL;
            $current_course_item = array('subject_area_id' => $subjectAreaId,  'catalog_number' => $catalogNum);
            $courseIdArray = array_keys($courseIds, $current_course_item); 

            if(!empty($courseIdArray)){
                $courseId = $courseIdArray[0];
            }

            //get class section id
            $classId = NULL;
            $current_class_item = array('course_id' => $courseId, 'term_id'=> $termId, 'class_number'=> $class_number);
            $classIdArray = array_keys($classIds, $current_class_item); 

            if(!empty($classIdArray)){
                $classId = $classIdArray[0];
            }

            //check if item is in array with just class id and section number
            $current_section_item = array(
                'class_id' => $classId, 
                'section_number' => $sectionNum, 
                'section_id' => $sectionId, 
                'enrollment_status' => $enrollmentStatus
            );
            $section_key = array_keys($sections_to_delete, $current_section_item); 
            $section_id = NULL;

            if(!empty($section_key)){
                //remove from array
                $section_id = $section_key[0];
                unset($sections_to_delete[$section_id]);
                
                //get section ids to delete from section_instructor
                //$section_instr_to_delete[] = $section_id;

            }
            else{
                //if it doesnt exist in array
                //add to database

                $sql = "INSERT INTO class_section(class_id, section_number, section_id, enrollment_status) VALUES($classId, '$sectionNum', $sectionId, '$enrollmentStatus')";
                if ($this->db_conn->query($sql) === TRUE) {
                    $section_id = $this->db_conn->insert_id;
                }
            }

            if(!is_null($section_id)){
                //new instructor info using $section_id and uclaid
                $instr_collection = $section->instructorUCLAIDCollection;

                foreach($instr_collection as $instr){

                    $uclaid = $instr->uclaid;
                    //add leading zeros
                    $uclaid = str_pad($uclaid, 9, '0', STR_PAD_LEFT);

                    $secInstr_key = array();
                    if(isset($section_instr_to_delete[$section_id])){

                        $secInstr_key = array_keys($section_instr_to_delete[$section_id], $uclaid); 
                    }
                    

                    //if exists in to delete
                    if(!empty($secInstr_key)){
                        //remove from array
                        unset($section_instr_to_delete[$section_id][$secInstr_key[0]]);
                    }
                    else{ //if it doesnt exist in array
                        $section_instr_str .= "($section_id, '$uclaid'),";
                    }
                }
            }
        }

        //Delete what is left in array from database sections table and propegating tables
        if(!empty($sections_to_delete)) {

            $sections_id_string =  implode(", ",array_keys($sections_to_delete));
            $sql = "DELETE FROM class_section WHERE id in (".$sections_id_string.")";
            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Sections deleted successfully");
                //class and class section dependancies are deleted automatically with cascade
            } else {
                $this->data_logging("Error deleting sections: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No sections to delete");
        }

        //insert section instructors
        if(!empty($section_instr_str)){

            $sql = "INSERT INTO section_instructor(class_section_id, uclaid) VALUES ".rtrim($section_instr_str,",");

            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Section Instructors inserted successfully");
            } else {
                $this->data_logging("Error inserting section instructors: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No new section instructors to insert");
        }

        $uclaidArray = array();

        //get ucla ids with no instructor information
        $sql = "SELECT 
                    DISTINCT uclaid 
                FROM section_instructor
                WHERE uclaid NOT IN (SELECT uclaid FROM instructor)";

        $result = $this->db_conn->query($sql);

        while($row = $result->fetch_assoc()) {
            $uclaidArray[] = (string)$row["uclaid"];
        }     

        $this->instructorUCLAIDs = array_unique($uclaidArray);

        $this->data_logging("Instructors count: ".count($this->instructorUCLAIDs));
    }

    protected function update_instructors(){

        $UCLAIDs = $this->instructorUCLAIDs;

        $instructors_in_db = array();

        //get existing instructors
        $sql = "SELECT uclaid FROM instructor";
        
        $result = $this->db_conn->query($sql);
        while($row = $result->fetch_assoc()) {
            $instructors_in_db[] = $row['uclaid'];
        }

        $newUCLAIDs = array_diff($UCLAIDs, $instructors_in_db);

        $insertInstructors = "";

        foreach ($newUCLAIDs as $UCLAID) {
            
            $jsonInstructor = $this->get_instructor($UCLAID);

            if(!is_null($jsonInstructor)){

                $nameCollection = $jsonInstructor->nameCollection[0];

                $firstName = addslashes($nameCollection->firstName);
                $middleName = addslashes($nameCollection->middleName);
                $lastName = addslashes($nameCollection->lastName);
                $fullName = addslashes($nameCollection->fullName);

                $insertInstructors .= "('$UCLAID', '$firstName', '$middleName', '$lastName', '$fullName'),";
            }
        }

        if(!empty($insertInstructors)){

            //insert new instructors
            $sql = 'INSERT INTO instructor (uclaid, first_name, middle_name, last_name, full_name) VALUES '.rtrim($insertInstructors,",");
            if ($this->db_conn->query($sql) === TRUE) {
                $this->data_logging("Instructor information inserted successfully");
            } else {
                $this->data_logging("Error inserting instructor information: " . $this->db_conn->error);
            }
        }
        else{
            $this->data_logging("No new instructor information to insert");
        }
    }
}