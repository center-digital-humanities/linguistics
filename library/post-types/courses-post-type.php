<?php
// Courses Post Type Settings

// add custom categories
register_taxonomy( 'quarter', 
	array('courses'), /* if you change the name of register_post_type( 'courses', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Quarters', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Quarter', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Quarters', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Quarters', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Quarter', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Quarter:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Quarter', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Quarter', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Quarter', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Quarter Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'quarter' )
	)
);

// let's create the function for the custom type
function courses_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'courses', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Courses', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Course', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Courses', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Course', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Course', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Course', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Course', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Courses', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No courses added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all department courses', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-welcome-learn-more', /* the icon for the custom post type menu */
			'with_front' => false,
			'has_archive' => false,
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'excerpt', 'revisions')
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'courses_post_type');
?>