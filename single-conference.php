<?php get_header(); ?>
			<div class="content">
				<div class="col main" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<?php if ( $post->post_parent ) { ?>
						<h1 class="single-title" itemprop="headline"><?php the_title(); ?></h1>
						<?php } ?>
						<section class="entry-content" itemprop="articleBody">
							<?php the_content(); ?>
							<? if(get_field('page_type') == "schedule") {
								if( have_rows('schedule') ) :
							    	while ( have_rows('schedule') ) : the_row();
							    		if( get_row_layout() == 'time_block' ) 
							    			get_template_part('snippets/conf', 'schedule');
							    	endwhile;
							    endif;
							} 
							if(get_field('page_type') == "speakers") { 
								if( have_rows('speakers') ): ?>
							
								<ul class="speakers">
							
								<?php while( have_rows('speakers') ): the_row(); 
									$speaker_name = get_sub_field('name');
									$speaker_bio = get_sub_field('bio');
									$speaker_photo = get_sub_field('speaker-photo');
									
									// thumbnail
									$size = 'speaker-photo';
									$thumb = $speaker_photo['sizes'][ $size ];
									$width = $speaker_photo['sizes'][ $size . '-width' ];
									$height = $speaker_photo['sizes'][ $size . '-height' ];
								?>
									<li>
									<?php if(get_field('show_photos') == "yes") { ?>
										<?php if( $speaker_photo ): ?>
										<img src="<?php echo $thumb; ?>" alt="Photo of <?php echo $speaker_name; ?>" class="<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php else: ?>
										<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
										<?php endif; ?>
									<?php } ?>
										<div class="speaker-details <?php if(get_field('show_photos') == "no") { ?>full<?php } ?>">
										<?php if( $speaker_name ): ?>
											<h4><?php echo $speaker_name; ?></h4>
										<?php endif; ?>
										<?php if( $speaker_bio ): ?>
											<?php echo $speaker_bio; ?>
										<?php endif; ?>
										</div>
									</li>
								<?php endwhile; ?>
								</ul>
								<?php endif; ?>
							<?php } ?>	
						</section>
					</article>

				<?php endwhile; else : ?>
				
					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>
				
				</div>
			</div>

<?php get_footer(); ?>