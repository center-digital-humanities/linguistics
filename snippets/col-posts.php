<div class="col news-col <?php the_sub_field('posts_width'); ?>">
	<h3><?php the_sub_field('posts_title'); ?></h3>
	<?php $term = get_sub_field('category');
		$amount = get_sub_field('amount_to_show');
		$posts_query = new WP_Query( array( 'showposts' => $amount, 'cat' => $term->term_id ) ); ?>
	<ol <?php if(get_sub_field('show_categories') == "yes") { ?> class="categories" <?php } ?>>
		<?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>
		<?php if(get_sub_field('show_categories') == "no") { ?><a href="<?php the_permalink() ?>"><?php } ?>
			<li>
				<?php 
					if(get_sub_field('posts_width') == "two") {
						if(get_sub_field('show_image') == "yes") {
							if(get_sub_field('show_categories') == "yes") { ?>
								<a href="<?php the_permalink() ?>">
							<?php }
								if ( has_post_thumbnail() ) {
									$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
									$url = $thumb['0']; ?>
									<img src="<?=$url?>" alt="<?php the_title(); ?>" />
								<?php } else { ?>
							        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-thumb.jpg" alt="A photo of <?php the_title(); ?>" />
							<?php }
							if(get_sub_field('show_categories') == "yes") { ?>
								</a>
							<?php }
						}
					} 
				?>
				<div class="item">
				<?php if(get_sub_field('show_categories') == "yes") { ?>
					<span class="category"><?php the_category( ', ' ); ?></span>
				<?php } ?>
					<?php if(get_sub_field('show_categories') == "yes") { ?><a href="<?php the_permalink() ?>"><?php } ?>
					<h4><?php the_title(); ?></h4>
					<p>
						<span class="post-date"><?php the_modified_time('M j, Y'); ?></span> - <?php $content = get_the_content();
						$trimmed_content = wp_trim_words( $content, 27, '...' );
						echo $trimmed_content; ?>
					</p>
					<?php if(get_sub_field('show_categories') == "yes") { ?></a><?php } ?>
				</div>
			</li>
		<?php if(get_sub_field('show_categories') == "no") { ?></a><?php } ?>
		<?php endwhile; ?>
	</ol>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	<?php if( $term ) { ?>
	<a class="btn" href="/category/<?php echo $term->slug; ?>/">View All<span class="hidden"> <?php echo $term->name; ?></span></a>
	<?php } else { ?>
	<a class="btn" href="/category/news/">View All<span class="hidden"> News</span></a>
	<?php } ?>
</div>