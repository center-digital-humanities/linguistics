<?php
/*
 Template Name: List Template
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>					
					<?php if( have_rows('list') ): ?>
						<ul>
						<?php while( have_rows('list') ): the_row(); 
							// vars
							$item_title = get_sub_field('item_title');
							$item_string = str_replace(array(".",",","-","(",")","'",":"," "), '' , $item_title);
							?>
							<li>
								<a href="#<?php echo $item_string; ?>"><?php echo $item_title; ?></a>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<?php if( have_rows('list') ): ?>
						<ul class="item-list">					
						<?php while( have_rows('list') ): the_row(); 
							// vars
							$item_title = get_sub_field('item_title');
							$item_string = str_replace(array(".",",","-","(",")","'",":"," "), '' , $item_title);
							$item_description = get_sub_field('description');
							$item_link = get_sub_field('link');
							
							$secondary_link_type = get_sub_field('secondary_link_type');
							$secondary_link_title = get_sub_field('secondary_link_title');
							$external_link = get_sub_field('external_link');
							$internal_link = get_sub_field('internal_link');
							$file = get_sub_field('file');
							
							$tertiary_link_type = get_sub_field('tertiary_link_type');
							$tertiary_link_title = get_sub_field('tertiary_link_title');
							$external_link_tertiary = get_sub_field('external_link_tertiary');
							$internal_link_tertiary = get_sub_field('internal_link_tertiary');
							$file_tertiary = get_sub_field('file_tertiary');
							
							?>
							<li id="<?php echo $item_string ?>" class="item">
								<h3>
									<?php if( $item_link ): ?>
									<a href="<?php echo $item_link; ?>">
									<?php endif; ?>
										<?php echo $item_title; ?>
									<?php if( $item_link ): ?>
									</a>
									<?php endif; ?>
								</h3>
								<?php echo $item_description; ?>
								<?php if( $item_link ): ?>
								<a href="<?php echo $item_link; ?>" class="btn">Learn More<span class="hidden"> About <?php echo $item_title; ?></span></a>
								<?php endif; ?>
								<?php if( $secondary_link_title ): ?>
								<?php if( $secondary_link_type == 'internal' ): ?>
								<a href="<?php echo $internal_link; ?>" class="additional internal">
								<?php endif; ?>
								<?php if( $secondary_link_type == 'external' ): ?>
								<a href="<?php echo $external_link; ?>" class="additional external">
								<?php endif; ?>
								<?php if( $secondary_link_type == 'file' ): ?>
								<span class="additional download"><a href="<?php echo $file; ?>"><?php endif; ?><?php echo $secondary_link_title; ?><?php if( $secondary_link_type ): ?></a></span>
								<?php endif; ?>
								<?php endif; ?>
								
								<?php if( $tertiary_link_title ): ?><br />
								<?php if( $tertiary_link_type == 'internal' ): ?>
								<a href="<?php echo $internal_link_tertiary; ?>" class="additional internal">
								<?php endif; ?>
								<?php if( $tertiary_link_type == 'external' ): ?>
								<a href="<?php echo $external_link_tertiary; ?>" class="additional external">
								<?php endif; ?>
								<?php if( $tertiary_link_type == 'file' ): ?>
								<span class="additional download"><a href="<?php echo $file_tertiary; ?>"><?php endif; ?><?php echo $tertiary_link_title; ?><?php if( $tertiary_link_type ): ?></a></span>
								<?php endif; ?>
								<?php endif; ?>
							</li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>