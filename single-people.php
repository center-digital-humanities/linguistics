<?php get_header(); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php
				if ( $post->post_parent ) {
				// If this is a child page
				 $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->post_parent, 'echo' => 0, 'post_type' => 'people', 'sort_column' => 'menu_order' ) );
				    // Make nav appear if only if there is anything to show
				    if(get_field('research', $post->post_parent) || get_field('dissertations', $post->post_parent) || get_field('publications', $post->post_parent) || get_field('courses', $post->post_parent) || get_field('custom_section', $post->post_parent) || get_field('cv', $post->post_parent) || get_field('personal_website', $post->post_parent) || get_field('academia_profile', $post->post_parent) || get_field('additional_link', $post->post_parent) || $post->post_parent) { ?>
				    <nav class="person-nav stick" role="navigation" aria-labelledby="person navigation">
				    	<div class="content">
				    		<ul>
				    			<?php if( empty( $post->post_content) ) {
				    			// If there is no bio, don't show bio link
				    			} else { ?>
				    			<li><a href="<?php echo get_permalink($post->post_parent); ?>">Bio</a></li>
				    			<?php } 
				    			if(get_field('research', $post->post_parent)) { ?>
				    			<li><a href="<?php echo get_permalink($post->post_parent); ?>/#research">Research</a></li>
				    			<?php } 
				    			if(get_field('publications', $post->post_parent)) { ?>
				    			<li><a href="<?php echo get_permalink($post->post_parent); ?>/#publications">Publications</a></li>
				    			<?php } 
				    			if(get_field('dissertations', $post->post_parent)) { ?>
				    			<li><a href="<?php echo get_permalink($post->post_parent); ?>/#dissertations">Dissertation</a></li>
				    			<?php } 
				    			if(get_field('courses', $post->post_parent)) { ?>
				    			<li><a href="<?php echo get_permalink($post->post_parent); ?>/#courses">Courses</a></li>
				    			<?php } 
				    			if( have_rows('custom_section', $post->post_parent) ): while( have_rows('custom_section', $post->post_parent) ): the_row(); 
				    				// vars
				    				$custom_title = get_sub_field('custom_title', $post->post_parent);
				    				$jump_link = str_replace(array(".",",","-","(",")","'",":"," "), '' , $custom_title);
				    			?>
				    			<li><?php if( $custom_title ): ?><a href="<?php echo get_permalink($post->post_parent); ?>/#<?php echo $jump_link ?>"><?php echo $custom_title ?></a><?php endif; ?></li>
				    			<?php endwhile; endif;
				    			if ( $children ) : 
				    			echo $children;
				    			endif;
				    			if(get_field('cv', $post->post_parent)) { ?>
				    			<li class="download"><a href="<?php the_field('cv', $post->post_parent); ?>">Download CV</a></li>
				    			<?php }
				    			if(get_field('personal_website', $post->post_parent)) { ?>
				    			<li class="link"><a href="<?php the_field('personal_website', $post->post_parent); ?>">Personal Website</a></li>
				    			<?php }
				    			if(get_field('academia_profile', $post->post_parent)) { ?>
				    			<li class="link"><a href="<?php the_field('academia_profile', $post->post_parent); ?>">Academia Profile</a></li>
				    			<?php }
				    			if(get_field('additional_link', $post->post_parent)) { ?>
				    			<li class="link"><a href="<?php the_field('additional_link', $post->post_parent); ?>"><?php the_field('additional_link_title', $post->post_parent); ?></a></li>
				    			<?php } ?>
				    		</ul>
				    	</div>
				    </nav>
				    <?php }
				} else {
				 $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->ID, 'echo' => 0, 'post_type' => 'people', 'sort_column' => 'menu_order' ) );
				// Make nav appear if only if there is anything to show
				if(get_field('research') || get_field('dissertations') || get_field('publications') || get_field('courses') || get_field('custom_section') || get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link') || !empty($children)) { ?>
				<nav class="person-nav stick" role="navigation" aria-labelledby="person navigation">
					<div class="content">
						<ul>
							<?php if( empty( $post->post_content) ) {
							// If there is no bio, don't show bio link
							} else { ?>
							<li><a href="#bio">Bio</a></li>
							<?php }
							if(get_field('research')) { ?>
							<li><a href="#research">Research</a></li>
							<?php }
							if(get_field('publications')) { ?>
							<li><a href="#publications">Publications</a></li>
							<?php } 
							if(get_field('dissertations')) { ?>
							<li><a href="#dissertations">Dissertation</a></li>
							<?php } 
							if(get_field('courses')) { ?>
							<li><a href="#courses">Courses</a></li>
							<?php }
							if( have_rows('custom_section') ): while( have_rows('custom_section') ): the_row(); 
								// vars
								$custom_title = get_sub_field('custom_title');
								$jump_link = str_replace(array(".",",","-","(",")","'",":"," "), '' , $custom_title);
							?>
							<li><?php if( $custom_title ): ?><a href="#<?php echo $jump_link ?>"><?php echo $custom_title ?></a><?php endif; ?></li>
							<?php endwhile; endif;
							if ( $children ) : 
							echo $children;
							endif;
							if(get_field('cv')) { ?>
							<li class="download"><a href="<?php the_field('cv'); ?>">Download CV</a></li>
							<?php }
							if(get_field('personal_website')) { ?>
							<li class="link"><a href="<?php the_field('personal_website'); ?>">Personal Website</a></li>
							<?php }
							if(get_field('academia_profile')) { ?>
							<li class="link"><a href="<?php the_field('academia_profile'); ?>">Academia Profile</a></li>
							<?php }
							if(get_field('additional_link')) { ?>
							<li class="link"><a href="<?php the_field('additional_link'); ?>"><?php the_field('additional_link_title'); ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</nav>
				<?php }
				} ?>
				<?php 
				// Do this for child people pages
				if ( $post->post_parent ) { ?>
				<div class="content main person-child">
					<?php if(get_field('photo', $post->post_parent)) {
						$image = get_field('photo', $post->post_parent);
						if( !empty($image) ): 
						// vars
						$url = $image['url'];
						$title = $image['title'];
						// thumbnail
						$size = 'people-large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];
					endif; ?>
					<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
					<?php } else { ?>
					<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
					<?php } ?>
					<div class="col" id="main-content" role="main">
						<a href="<?php echo get_permalink($post->post_parent); ?>">
							<span class="pre-title"><?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?></span>
						</a>
					   <h1><?php the_title(); ?></h1>
					   <?php the_content(); ?>
					</div>
				</div>
				<?php } 
				// Otherwise do all of this...
				else {
				?>
				<header class="bio" id="bio">
					<div class="content">
						<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
							// vars
							$url = $image['url'];
							$title = $image['title'];
							// thumbnail
							$size = 'people-large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
						endif; ?>
						<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } ?>
						<section>
							<h1>
							<?php the_title(); ?></h1>
							<?php if(get_field('position_title')) { ?>
							<span class="position"><?php the_field('position_title'); ?></span>
							<?php } ?>
							<?php if(get_field('education')) { ?>
							<span class="education"><?php the_field('education'); ?></span>
							<?php } ?>
							<div class="details">
							<?php if(get_field('email_address')) { ?>
								<?php $person_email = antispambot(get_field('email_address')); ?>
								<span><strong>E-mail:</strong> <a href="mailto:<?php echo $person_email; ?>"><?php echo $person_email; ?></a></span>
							<?php } ?>
							<?php if(get_field('phone_number')) { ?>
								<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
							<?php } ?>
							<?php if(get_field('office')) { ?>
								<span><strong>Office: </strong><?php the_field('office'); ?></span>
							<?php } ?>
							<?php if(get_field('office_hours')) { ?>
								<p><strong>Office Hours: </strong><?php the_field('office_hours'); ?></p>
							<?php } ?>
							<?php if(get_field('pronouns')) { ?>
								<p><strong>Pronouns: </strong><?php the_field('pronouns'); ?></p>
							<?php } ?>
							</div>
							<?php the_content(); ?>
						</section>
					</div>
				</header>
				<?php //Mobile menu
				$children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->ID, 'echo' => 0, 'post_type' => 'people', 'sort_column' => 'menu_order' ) );
				// Make nav appear if only if there is anything to show
				if(get_field('research') || get_field('dissertations') || get_field('publications') || get_field('courses') || get_field('custom_section') || get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link') || !empty($children)) { ?>
				<nav class="person-nav mobile" role="navigation" aria-labelledby="person navigation">
					<div class="content">
						<h3>Sections:</h3>
						<ul>
							<?php if( empty( $post->post_content) ) {
							// If there is no bio, don't show bio link
							} else { ?>
							<li><a href="#bio">Bio</a></li>
							<?php }
							if(get_field('research')) { ?>
							<li><a href="#research">Research</a></li>
							<?php }
							if(get_field('publications')) { ?>
							<li><a href="#publications">Publications</a></li>
							<?php } 
							if(get_field('dissertations')) { ?>
							<li><a href="#dissertations">Dissertation</a></li>
							<?php } 
							if(get_field('courses')) { ?>
							<li><a href="#courses">Courses</a></li>
							<?php }
							if( have_rows('custom_section') ): while( have_rows('custom_section') ): the_row(); 
								// vars
								$custom_title = get_sub_field('custom_title');
								$jump_link = str_replace(array(".",",","-","(",")","'",":"," "), '' , $custom_title);
								
							?>
							<li><?php if( $custom_title ): ?><a href="#<?php echo $jump_link ?>"><?php echo $custom_title ?></a><?php endif; ?></li>
							<?php endwhile; endif;
							if ( $children ) : 
							echo $children;
							endif;
							if(get_field('cv')) { ?>
							<li class="download"><a href="<?php the_field('cv'); ?>">Download CV</a></li>
							<?php }
							if(get_field('personal_website')) { ?>
							<li class="link"><a href="<?php the_field('personal_website'); ?>">Personal Website</a></li>
							<?php }
							if(get_field('academia_profile')) { ?>
							<li class="link"><a href="<?php the_field('academia_profile'); ?>">Academia Profile</a></li>
							<?php }
							if(get_field('additional_link')) { ?>
							<li class="link"><a href="<?php the_field('additional_link'); ?>"><?php the_field('additional_link_title'); ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</nav>
				<?php }
				 ?>
				<div class="content main">
					<div class="col" id="main-content" role="main">
						<?php if(get_field('research')) { ?>
						<section id="research">
							<h2>Research</h2>
							<hr />
							<?php the_field('research'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('books') || get_field('articles') || get_field('publications') || get_field('additional_works') || get_field('dissertations')) { ?>
						<section id="publications">
							<h2>Publications</h2>
							<hr />
							<?php if(get_field('books')) { ?>
							<section id="books">
								<h3>Books</h3>
								<?php $book = get_field('books'); ?>
								<ul class="book-list">
									<? if( $book ): ?>
									<?php foreach( $book as $post): ?>
									<?php setup_postdata($post); ?>
									<li>
										<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										<?php if(get_field('book_cover')) {
											$image = get_field('book_cover');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'small-book';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
											endif; ?>
											<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
											<?php } else { ?>
												<div class="custom-cover cover">
													<span class="title"><?php the_title(); ?></span>
												</div>
											<?php } ?>
										</a>
										<dl>
											<dt class="title">
												<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
											</dt>
											<?php if(get_field('subtitle')) { ?>
											<dd class="subtitle">
												<?php the_field('subtitle'); ?>
											</dd>
											<?php } ?>
											<?php if(get_field('publisher')) { ?>
											<dd class="publisher">
												<?php the_field('publisher'); ?>, <?php the_field('published_date'); ?>
											</dd>
											<?php } ?>
										</dl>
									</li>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</ul>
							</section>
							<?php } ?>
							<?php if(get_field('articles')) { ?>
							<section id="articles">
								<h3>Articles</h3>
							<?php the_field('articles'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('publications')) { ?>
							<section id="additional_publications">
							<?php the_field('publications'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('dissertations')) { ?>
							<section id="dissertations">
								<h3>Dissertation</h3>
								<?php the_field('dissertations'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('additional_works')) { ?>
							<section id="works">
								<?php the_field('additional_works'); ?>
							</section>
							<?php } ?>
						</section>
						<?php } ?>
						<?php if(get_field('awards')) { ?>
						<section id="awards">
							<h2>Honors & Awards</h2>
							<hr />
							<?php the_field('awards'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('courses')) { ?>
						<section id="courses">
							<h2>Courses</h2>
							<hr />
							<?php the_field('courses'); ?>
						</section>
						<?php } ?>
						<?php if( have_rows('custom_section') ): while( have_rows('custom_section') ): the_row(); 
							// vars
							$custom_title = get_sub_field('custom_title');
							$custom_content = get_sub_field('custom_content');
							$jump_id = str_replace(array(".",",","-","(",")","'",":"," "), '' , $custom_title);
						?>
						<section id="<?php echo $jump_id ?>">
						<?php if( $custom_title ): ?>
							<h2><?php echo $custom_title ?></h2>
							<hr />
						<?php endif; ?>
						<?php if( $custom_content ): ?>
							<?php echo $custom_content ?>
						<?php endif; ?>
						</section>
						<?php endwhile; endif; ?>
					</div>
				</div>
			<?php }
			// Ending else here
			endwhile; ?>
			<?php else : endif; ?>
<?php get_footer(); ?>